var allItems = ItemManager.getAll();
var allSets = west.storage.ItemSetManager.getAll();
var ignoredSets = [
    //Сетове на играчи
    'set_goodnight',
    'set_meischdas',
    'set_snowi',
    'set_akoya',
    'set_dark_templar',
    'set_giony4you',
    'set_matsacolthan',
    'set_multi_account',
    'set_orichyto',
    'set_ilpresidento',
    'set_stelladelwest',
    'set_merlin',
    'set_lordlamar',
    'set_svetlanaili',
    'set_lucabadoer',
    'set_luckyday',
    'set_texasladys',
    'set_meta',
    'set_hassan',
    'set_dorafix',
    'set_txankete',
    'set_chinski',
    'set_xque69',
    'set_montxi',
    'set_gogoboom',
    'set_sar_pepita',
    'set_r0mpehues0s',
    'set_damed',
    'set_flopsinchen',
    'set_kolac015',
    'set_deanie_obanion',
    'set_mrs_dreamer',
    'set_wolfskin',
    'set_childerich',
    'set_killstreak1',
    'set_olga',
    'set_jerznero',
    'set_gobnit',
    'set_rambo',
    'set_volka',
    'set_rompehuesos',
    'set_agetn_pinky',
    'set_agent_brain',
    'set_ninja123',
    // /Край на сетовете на играчи
    'tw_times_set',
    'set_independence_gun_winner',
    'set_octoberfest_gun_winner',
    'set_4july_2014_weapon_ranking_winner',
    'set_octoberfest_2014_weapon_ranking_winner',
    'set_speedworld_2014',
    'set_easter_2015_5',
    'set_4july_2015_weapon_ranking_winner',
    'set_october_2015_winner',
    'set_easter_2016_5w',
    'set_sale_2016_1',
    'set_oktoberfest_2016_5',
    'set_wrightbrothers'
];
var config = {
    'showIgnoredSets': false,
    'showCompletedSets': false,
    'showItems': true,
    'showItemsInPosession': false,
};

var countItem = function(itemBaseId) {
    wearItemIds = Wear.item_ids.map((id) => {
        return Math.floor(id / 1000);
    });

    count = 0;
    if (wearItemIds.indexOf(itemBaseId) >= 0) {
        count += 1;
    }

    var itemsInBag = Bag.getItemsByBaseItemId(itemBaseId);
    if (itemsInBag) {
        for (var i in itemsInBag) {
            if (itemsInBag.hasOwnProperty(i)) {
                count += itemsInBag[i].count;
            }
        }
    }

    return count;
};

for (var i in allSets) {
    if (allSets.hasOwnProperty(i)) {
        var set = allSets[i];
        if (!config.showIgnoredSets && ignoredSets.indexOf(set.key) >= 0) {
            continue;
        }
        
        var setStats = {"completed": 0, "total": 0};
        var logs = [];
        for (var j in set.items) {
            if (set.items.hasOwnProperty(j)) {
                setStats.total += 1;
                var itemBaseId = set.items[j];
                var count = countItem(itemBaseId);
                if (count > 0) {
                    setStats.completed += 1;
                    if (config.showItemsInPosession) {
                        logs.push(["    %c" + allItems[itemBaseId].name + ' [' + count + ']', 'color: green;']);
                    }
                } else {
                    logs.push(["    %c" + allItems[itemBaseId].name, 'color: red;']);
                }
            }
        }

        var showItemsLogs = false;
        if (setStats.completed === setStats.total) {
            if (config.showCompletedSets) {
                showItemsLogs = true;
                console.log('%c ' + set.name + ' [' + setStats.completed + '/' + setStats.total + '] (' + set.key + ')', 'font-weight: bold; font-size: 120%; color: green;');
            }
        } else {
            showItemsLogs = true;
            console.log('%c ' + set.name + ' [' + setStats.completed + '/' + setStats.total + '] (' + set.key + ')', 'font-weight: bold; font-size: 120%; color: red;');
        }
        
        if (config.showItems && showItemsLogs) {
            for (var i in logs) {
                if (logs.hasOwnProperty(i)) {
                    console.log(logs[i][0], logs[i][1]);
                }
            }
        }

    }
}