var allItems = ItemManager.getAll();
var allSets = west.storage.ItemSetManager.getAll();
var ignoredSets = [
    //Сетове на играчи
    'set_goodnight',
    'set_meischdas',
    'set_snowi',
    'set_akoya',
    'set_dark_templar',
    'set_giony4you',
    'set_matsacolthan',
    'set_multi_account',
    'set_orichyto',
    'set_ilpresidento',
    'set_stelladelwest',
    'set_merlin',
    'set_lordlamar',
    'set_svetlanaili',
    'set_lucabadoer',
    'set_luckyday',
    'set_texasladys',
    'set_meta',
    'set_hassan',
    'set_dorafix',
    'set_txankete',
    'set_chinski',
    'set_xque69',
    'set_montxi',
    'set_gogoboom',
    'set_sar_pepita',
    'set_r0mpehues0s',
    'set_damed',
    'set_flopsinchen',
    'set_kolac015',
    'set_deanie_obanion',
    'set_mrs_dreamer',
    'set_wolfskin',
    'set_childerich',
    'set_killstreak1',
    'set_olga',
    'set_jerznero',
    'set_gobnit',
    'set_rambo',
    'set_volka',
    'set_rompehuesos',
    'set_agetn_pinky',
    'set_agent_brain',
    'set_ninja123',
    // /Край на сетовете на играчи
    'tw_times_set',
    'set_independence_gun_winner',
    'set_octoberfest_gun_winner',
    'set_4july_2014_weapon_ranking_winner',
	'set_easter_2014_weapon_ranking_winner',
    'set_octoberfest_2014_weapon_ranking_winner',
    'set_speedworld_2014',
    'set_easter_2015_5',
    'set_4july_2015_weapon_ranking_winner',
    'set_october_2015_winner',
    'set_easter_2016_5w',
    'set_sale_2016_1',
    'set_oktoberfest_2016_5',
    'set_wrightbrothers',
	'set_leveling_valentin_2014',
	'instance_set_1',
	//Не особено важни сетове:
	'set_dancer',
	'set_farmer',
	'set_gentleman',
	'set_indian',
	'set_mexican',
	'set_pilgrim_male',
	'set_pilgrim_female',
	'set_quackery',
	'season_set',
	'greenhorn_set',
	'set_shop_low',
	'set_shop_mid',
	'set_shop_high',
	'set_shop_adventure',
	'set_shop_duel',
	'set_shop_work',
	'set_shop_soldier',
	'set_dayofthedead_2014_1',
	'set_xmas2015_clothing',
	'set_free_to_use_dummy',
	'set_colcord',
	//Трудни за намиране сетове:
	'set_duelist_gun',
	'set_fort_gun',
	'set_fortunehunter_gun',
	'set_meleeduelist_horse',
	'set_rangedduelist_horse',
	'set_proworker_horse',
	'set_independence_gun_1',
	'set_independence_gun_2',
	'set_independence_gun_3',
	'set_octoberfest_gun_1',
	'set_octoberfest_gun_2',
	'set_octoberfest_gun_3',
	'set_xmas2013_tool',
	'set_valentin_2014',
	'set_valentin_gun_2014',
	'set_veteran_horse',
	'set_easter_2014_weapon_1',
	'set_easter_2014_weapon_2',
	'set_easter_2014_weapon_3',
	'set_4july_2014_animal_1',
	'set_4july_2014_animal_2',
	'set_4july_2014_animal_3',
	'set_october_2014_animal_1',
	'set_october_2014_animal_2',
	'set_october_2014_animal_3',
	'set_dayofthedead_2014_animal_3',
	'set_dayofthedead_2014_animal_4',
	'set_dayofthedead_2014_weapon_3',
	'set_dayofthedead_2014_weapon_4',
	'set_4july_2015_animal_1',
	'set_4july_2015_animal_2',
	'set_4july_2015_animal_3',
	'set_october_2015_1_weapon',
	'set_october_2015_2_weapon',
	'set_october_2015_3_weapon',
	'set_dotd_2015_2_weapon',
	'set_dotd_2015_3_weapon',
	'set_dotd_2015_3_animal',
	'xmas2015_horse',
	'set_easter_2016_1w',
	'set_easter_2016_2w',
	'set_easter_2016_3w',
	'set_4july_2016_2',
	'set_4july_2016_5',
	'set_oktoberfest_2016_1_weapon',
	'set_oktoberfest_2016_2_weapon',
	'set_oktoberfest_2016_3_weapon',
	'set_dotd_2016_2_animal',
	'set_dotd_2016_3_animal',
	'set_dotd_2016_3_weapon'
	
];
var config = {
    'showIgnoredSets': false,
    'showCompletedSets': false,
    'showItems': true,
    'showItemsInPosession': false,
};

var countItem = function(itemBaseId) {
    wearItemIds = Wear.item_ids.map((id) => {
        return Math.floor(id / 1000);
    });

    count = 0;
    if (wearItemIds.indexOf(itemBaseId) >= 0) {
        count += 1;
    }

    var itemsInBag = Bag.getItemsByBaseItemId(itemBaseId);
    if (itemsInBag) {
        for (var i in itemsInBag) {
            if (itemsInBag.hasOwnProperty(i)) {
                count += itemsInBag[i].count;
            }
        }
    }

    return count;
};

for (var i in allSets) {
    if (allSets.hasOwnProperty(i)) {
        var set = allSets[i];
        if (!config.showIgnoredSets && ignoredSets.indexOf(set.key) >= 0) {
            continue;
        }
        
        var setStats = {"completed": 0, "total": 0};
        var logs = [];
        for (var j in set.items) {
            if (set.items.hasOwnProperty(j)) {
                setStats.total += 1;
                var itemBaseId = set.items[j];
                var count = countItem(itemBaseId);
                if (count > 0) {
                    setStats.completed += 1;
                    if (config.showItemsInPosession) {
                        logs.push(["    %c" + allItems[itemBaseId].name + ' [' + count + ']', 'color: green;']);
                    }
                } else {
                    logs.push(["    %c" + allItems[itemBaseId].name, 'color: red;']);
                }
            }
        }

        var showItemsLogs = false;
        if (setStats.completed === setStats.total) {
            if (config.showCompletedSets) {
                showItemsLogs = true;
                console.log('%c ' + set.name + ' [' + setStats.completed + '/' + setStats.total + '] (' + set.key + ')', 'font-weight: bold; font-size: 120%; color: green;');
            }
        } else {
            showItemsLogs = true;
            console.log('%c ' + set.name + ' [' + setStats.completed + '/' + setStats.total + '] (' + set.key + ')', 'font-weight: bold; font-size: 120%; color: red;');
        }
        
        if (config.showItems && showItemsLogs) {
            for (var i in logs) {
                if (logs.hasOwnProperty(i)) {
                    console.log(logs[i][0], logs[i][1]);
                }
            }
        }

    }
}